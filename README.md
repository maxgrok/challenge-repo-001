## README

This is an IBM Code Challenge. 

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### Suggested Improvements

Infinite scroll, cursor-based pagination, sorting by Max/Min Account Balance, sorting by Date, show transactions onClick of each account balance (if available) expanded in an accordion fashion. 
