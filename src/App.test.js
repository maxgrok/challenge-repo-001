import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Enzyme, {shallow} from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import { findByTestAttr } from './tests/testUtils';

Enzyme.configure({adapter: new EnzymeAdapter()});

describe('general tests for app', () =>{
    let setup = shallow(<App />)

    test('contains table', ()=>{
        const table = findByTestAttr(setup, 'table')
        expect(table.length).toBe(1)
    })

    test('has table headers', ()=>{
        const tableHeaders = findByTestAttr(setup, 'table-headers')
        expect(tableHeaders.length).toBe(1)
    })
    test('renders without crashing', () => {
        const wrapper = shallow(<App/>)
    });
})
