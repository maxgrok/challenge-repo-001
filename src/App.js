import React, { Component } from 'react';
import './App.css';
import axios from'axios';
import AccountList from './components/AccountsList'
import {Preloader} from 'react-materialize';
class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      accounts: [
      ]
    }
    this.accounts()

  }

  async accounts(){
    try {
      let accounts = await axios.get("https://bc-account-service.mybluemix.net/v1/accounts").then(res=>{this.setState({accounts: res.data})});
    } catch (error){
      return (<div>{error}</div>)
    }
  }
  
  render() {
    return (
      <div className="container">
        <table data-test="table" className="responsive-table highlight">
          <thead data-test="table-headers"> 
            <th style={{"fontWeight":"bold"}}>Table Header</th>
            <th style={{"fontWeight":"bold"}}>Corresponding JSON Key</th>
        </thead>
          {this.state.accounts < 1 ? 
    <div><Preloader flashing size="big" /><p>Loading...</p></div> : <AccountList accounts={this.state.accounts} />}
        </table>
        </div>
    );
  }
}

export default App;
