import React from 'react';
import { findByTestAttr } from './testUtils';
import Enzyme, {shallow} from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import AccountsList from '../components/AccountsList';

Enzyme.configure({adapter: new EnzymeAdapter()});
const defaultProps = {
    accounts: [{
        "accounts":[{   
            "account_id": "76833",
            "first_name": "Nova",
            "last_name": "Metz",
            "email": "Geovanni74@hotmail.com",
            "date_joined": "2018-06-11T13:14:31.082Z"
            }
    ]}]
}

const url = "https://bc-account-service.mybluemix.net/v1/accounts"
const setup = (props={}) =>{
    const setupProps = {...defaultProps, ...props}
    return shallow(<AccountsList {...defaultProps} />)
}
//test headers are there
    describe("receives all accounts as props", () =>{
        let wrapper; 

        test('fetches account_id and displays it along with account id header',()=>{
            //check to see if data-test=account_id is rendered
            wrapper = setup()
            const account_id = findByTestAttr(wrapper, 'account_id')
            expect(account_id.length).toBe(1);
            const account_id_header = findByTestAttr(wrapper, 'account_id_header');
            expect(account_id_header.length).toBe(1)
        });

        test('fetches email and displays it along with email header',()=>{
            const email = findByTestAttr(wrapper, 'email')
            expect(email.length).toBe(1);
            const email_header = findByTestAttr(wrapper, 'email_header');
            expect(email_header.length).toBe(1)
        });

        test('fetches first_name + " " + last_name and displays it along with name header',()=>{
            const name = findByTestAttr(wrapper, 'full_name')
            expect(name.length).toBe(1);
            const full_name_header = findByTestAttr(wrapper, 'full_name_header');
            expect(full_name_header.length).toBe(1)
        });

        test('fetches date_joined and displays it',()=>{
            const date_joined = findByTestAttr(wrapper, 'date_joined')
            expect(date_joined.length).toBe(1);
            const date_joined_header = findByTestAttr(wrapper, 'date_joined_header');
            expect(date_joined_header.length).toBe(1)
        });


        test('displays it balance header',()=>{
            //check to see if data-test=account_id is rendered
            wrapper = setup()
            const balance_header = findByTestAttr(wrapper, 'balance_header');
            expect(balance_header.length).toBe(1)
        });
    
    })

    