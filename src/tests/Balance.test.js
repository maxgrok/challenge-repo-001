import React from 'react';
import { findByTestAttr } from './testUtils';
import Enzyme, {shallow} from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import Balance from '../components/Balance';

Enzyme.configure({adapter: new EnzymeAdapter()});

const defaultProps ={
    balance: "$00.00"
}
const setup = (props={}) =>{
    const setupProps = {...defaultProps, ...props}
    return shallow(<Balance {...setupProps} />)
}

  describe("fetching balance works", ()=>{
    let wrapper;
    
    test('is passed the balance of one account and displays it',()=>{
        wrapper = setup()
        const balance = findByTestAttr(wrapper, 'balance')
        expect(balance.length).toBe(1)
    });
  })