import React,{Component} from 'react';
import Axios from 'axios';

class Balance extends Component
{
    constructor(props){
        super(props);
        this.state ={
            balance: "Loading...",
            error: "Error"
        }
        this.balance()
    }

    async balance(){
        if (!this.props.account_id){
            return (
                <p>Not Currently Available</p>
            )
        } else {
            try {
                let response = await Axios.get(`https://bc-account-service.mybluemix.net/v1/balance?account_id=${this.props.account_id}`).then(res=> {return res.data});
                console.log(response)
                this.setState({balance: response[0].balance});
              } catch (error) {
                this.setState({error})
                return (<div>{this.state.error}</div>)
              }
        }
    }
    getTotalCents =(dollarsAndCents) =>{
            let strBalance = dollarsAndCents.toString()
            let balance = parseFloat(strBalance).toFixed(2);
            if(isNaN(balance)){
                return this.state.error
            } else{
                return balance
           }
    }

    getBalance(){
        if (this.state.balance === "Loading..." && this.state.error !== "Error"){
            return <div>Loading...</div>
         }
         else if (this.state.error !== "Error"){
             return this.state.error
         }
        else{
            return this.getTotalCents(this.state.balance)
         }
    }
    render(){
    return (
        <div data-test="balance">
           ${this.getBalance()}
        </div>
    )
    }
}

export default Balance;