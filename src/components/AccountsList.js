import React from 'react';
import Balance from './Balance';

var moment = require('moment');

const AccountsList=(props)=>{
    let accounts = props.accounts.map((account, index)=>{       
        return (
                    <tbody>
                <tr>
                        <td data-test="account_id_header"><strong>Account ID</strong></td>
                        <td data-test="account_id">
                        {account.account_id}</td>
                </tr>
                  <tr>
                      <td data-test="email_header"><strong>Email</strong></td>
                      <td data-test="email">
                              {account.email}
                          </td>
                  </tr>
                  <tr>
                      <td data-test="full_name_header"><strong>Full Name</strong></td>
                      <td data-test="full_name">
                          {account.first_name}  {account.last_name}
                          </td>
                  </tr>
                  <tr>
                      <td data-test="date_joined_header"><strong>Date Joined</strong></td>
                      <td data-test="date_joined">
                          {moment(account.date_joined).format('MM Do YYYY')}
                      </td>
                  </tr>
                  <tr>
                      <td data-test="balance_header"><strong>Balance</strong></td>
                      <td data-test="balanceComponent"> <Balance account_id={account.account_id} /> </td>
                  </tr>
                  <tr>
                  <td></td>
                  <td></td>
                  </tr>
                  </tbody>
                    )
                })
    
    return accounts;
}
export default AccountsList;